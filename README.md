# Starter expressJS-typescript-typeorm

## Craft by Fabien Jégu

Steps to run this project:

1. Run `npm i` command
2. Setup database settings inside `ormconfig.json` file
3. Run `npm start` command
