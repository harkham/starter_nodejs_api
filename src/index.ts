import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response} from "express";
import routes from "./routes/routes";

const exphbs  = require('express-handlebars');
const path = require('path');
createConnection().then(async connection => {

    // create express app
    const app = express();
    app.use(bodyParser.json());

    console.log("SRC", path.join(__dirname, '/assets/sass/style.scss'));
    // setup the public directory
    app.use('/public',express.static(path.join(__dirname, 'public')));

    app.use("/", routes);
    
    // view engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'hbs');
    const hbsa = exphbs.create({defaultLayout: 'main'});
    app.engine('handlebars', hbsa.engine);
    app.set('view engine', 'handlebars');
    // setup express app here
    // ...

    // start express server
    app.listen(3000);

    console.log("Express server has started on port 3000.http://localhost:3000");

}).catch(error => console.log(error));
