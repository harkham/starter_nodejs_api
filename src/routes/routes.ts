import { Router, Request, Response } from "express";

import auth from "./auth";
import user from "./user";
import test from "./test";

const routes = Router();

routes.get("/", function(req, res) {
  res.render("welcome");
});
routes.use("/auth", auth);
routes.use("/user", user);
routes.use("/test", test);

// setup default routes
routes.get("*", function(req, res) {
  console.log("404");
  res.status(404).render("404");
});
export default routes;
