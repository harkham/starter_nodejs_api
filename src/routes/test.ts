import { Router } from "express";
import TestController from "../controllers/TestController";
import UserController from "../controllers/UserController"

const router = Router();
router.get("/", TestController.lol);

export default router;
