import { Request, Response } from "express";
export default class TestController{

    static lol = async (req: Request, res: Response) => {
        res.render('test', { lol: 'Nice !' });
    }
}