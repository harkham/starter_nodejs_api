import {MigrationInterface, QueryRunner} from "typeorm";
import { getRepository } from "typeorm";
import { User } from '../entity/User';

export class createAdminAccount implements MigrationInterface {
    
    async up(queryRunner: QueryRunner): Promise<any> {
        const userRepository = getRepository(User);
        let user = new User();
        user.username = "hark";
        user.password = "HoG1era1";
        user.role = "ADMIN";
        user.hashPassword();
        await userRepository.save(user);
    }

    async down(queryRunner: QueryRunner): Promise<any> { 
        await queryRunner.query(`ALTER TABLE "post" ALTER COLUMN "name" RENAME TO "title"`); // reverts things made in "up" method
    } 
}